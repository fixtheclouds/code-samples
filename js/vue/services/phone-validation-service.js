import settings from '@/lib/settings';

export default _.bindAll({
  checkFormat({ number }) {
    return !!number && this.numberFormat.test(number);
  },

  optionalFormatCheck(phone) {
    return !settings.instance !== 'Malaysia' || this.checkMalaysianPhone(phone);
  },

  checkMalaysianPhone(phone) {
    return this[`malaysian${phone.type.replace('Phone', '')}NumberFormat`].test(phone.number);
  },

  get numberFormat() {
    return new RegExp(`^\\${settings.callingCode}\\d{${settings.phoneMinLength},${settings.phoneMaxLength}}$`);
  },

  get malaysianMobileNumberFormat() {
    return new RegExp(`^${this.cleanCallingCode}1(1\\d{8}|[^1]\\d{7})$`);
  },

  get malaysianLineNumberFormat() {
    return new RegExp(`^${this.cleanCallingCode}(3\\d{8}|[^03]\\d{7})$`);
  },

  get cleanCallingCode() {
    return settings.callingCode.replace('+', '');
  }
}, ['checkFormat', 'optionalFormatCheck']);
