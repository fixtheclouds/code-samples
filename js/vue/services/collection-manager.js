import Vue from 'vue';
import history from '@/lib/history';
import i18n from '@/lib/i18n';
import * as ability from '@/lib/abilities';
import notify from '@/lib/notify';

const defaultAction = 'index';
const defaultPerPage = 10;
const defaultSortColumn = 'created_at';
const defaultSortDirection = 'desc';

export default class CollectionManager {
  constructor({ model, action, actionArgs, collectionName, filters, sorting, page, perPage, concatOnLoad, useQuery, canSelect, isSelectable }) {
    this.model = model;
    this.action = action || defaultAction;
    this.actionArgs = actionArgs || [];
    this.collectionName = collectionName || model.resource;
    this.page = page || 1;
    this.defaultFilters = filters || {};
    this.filters = _.cloneDeep(this.defaultFilters);
    this.defaultSorting = sorting || {};
    this.sorting = this.buildDefaultSorting();
    this.perPage = perPage || defaultPerPage;
    this.concatOnLoad = concatOnLoad || false;
    this.useQuery = useQuery === undefined ? true : useQuery;
    this.isSelectable = isSelectable || _.constant(false);
    this.collection = [];
    this.total = 0;
    this.loading = false;
    this.selectAll = false;

    this.checkCanSelect(canSelect);
    this.readQuery();
  }

  async load() {
    this.loading = true;

    this.updateQuery();

    try {
      const { data } = await this.model[this.action](...this.actionArgs, this.generateParams());
      this.processNewItems(data[this.collectionName]);
      this.total = data.total_count;
    } catch (ex) {
      notify.error(i18n.t('internal_server_error'));
    } finally {
      this.loading = false;
    }
  }

  add(item) {
    this.collection.unshift(item);
    this.total += 1;
  }

  update(item) {
    const index = _.findIndex(this.collection, { id: item.id });
    this.remove(item);
    this.collection.splice(index, 0, item);
    this.total += 1;
  }

  remove(item) {
    this.collection = _.differenceBy(this.collection, [item], 'id');
    this.total -= 1;
  }

  reset() {
    this.filters = _.cloneDeep(this.defaultFilters);
    this.sorting = this.buildDefaultSorting();
    this.page = 1;
    this.load();
  }

  get hasTotal() {
    return !_.isNil(this.total);
  }

  get selectableItems() {
    return this.collection.filter((item) => !this.isSelectable || this.isSelectable(item));
  }

  get selectedItems() {
    if (this.selectAll) {
      return this.selectableItems;
    }
    return _.filter(this.collection, 'isSelected');
  }

  get isAllSelected() {
    return this.selectableItems.length === this.selectedItems.length;
  }

  get isSomeSelected() {
    return !this.isAllSelected && this.selectedItems.length > 0;
  }

  triggerAll() {
    if (this.selectAll) return;
    const select = !this.isAllSelected;
    this.selectableItems.forEach((item) => {
      Vue.set(item, 'isSelected', select);
    });
  }

  unselectAll() {
    this.selectAll = false;
    this.selectableItems.forEach((item) => {
      Vue.set(item, 'isSelected', false);
    });
  }

  generateParams() {
    return {
      q: JSON.stringify(this.filters),
      page: this.page,
      per: this.perPage,
      'sort[table]': this.sorting.table,
      'sort[col]': this.sorting.column,
      'sort[dir]': this.sorting.direction
    };
  }

  processNewItems(items) {
    if (this.concatOnLoad) {
      this.collection = this.collection.concat(items);
    } else {
      this.collection = items;
    }
  }

  buildDefaultSorting() {
    const defaultTable = this.model.table;
    let { table, column, direction } = this.defaultSorting;

    return {
      get table() {
        return table || defaultTable;
      },
      set table(value) {
        table = value;
      },
      get column() {
        return column || defaultSortColumn;
      },
      set column(value) {
        column = value;
      },
      get direction() {
        return direction || defaultSortDirection;
      },
      set direction(value) {
        direction = value;
      }
    };
  }

  async checkCanSelect(query) {
    this.canSelect = !query;
    if (query) {
      this.canSelect = await ability.can(query);
    }
  }

  readQuery() {
    if (!this.useQuery) return;
    const { q, sorting, page } = history.getQuery();
    this.filters = q ? JSON.parse(q) : this.filters;
    this.sorting = sorting ? JSON.parse(sorting) : this.sorting;
    this.page = page ? parseInt(page) : this.page;
  }

  updateQuery() {
    if (!this.useQuery) return;
    const params = {
      q: null,
      sorting: null,
      page: null
    };
    if (!_.isEqual(this.filters, this.defaultFilters)) {
      params.q = JSON.stringify(this.filters);
    }
    if (!this.isDefaultSorting()) {
      params.sorting = JSON.stringify(this.sorting);
    }
    if (this.page !== 1) {
      params.page = this.page;
    }
    history.updateQuery(params);
  }

  isDefaultSorting() {
    const { sorting } = this;
    const defaultSorting = this.buildDefaultSorting();
    return (!sorting.table || sorting.table === defaultSorting.table) &&
      sorting.column === defaultSorting.column &&
      sorting.direction === defaultSorting.direction;
  }
}
