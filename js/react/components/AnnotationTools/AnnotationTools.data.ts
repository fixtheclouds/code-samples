import { TOOLS } from '../../../common/types';

export type ToolItem = {
  id: string;
  name?: string;
  isModeratorOnly?: boolean;
};

export const SWITCH = 'switch';
export const STUFF_SEPARATOR = 'separator';
export const PALETTE = 'palette';

export const TOOLS_DATA: ToolItem[] = [
  { id: TOOLS.PEN, name: 'pen' },
  { id: TOOLS.RECT, name: 'rect' },
  { id: TOOLS.ELLIPSE, name: 'ellipse' },
  { id: TOOLS.LINE, name: 'line' },
  { id: PALETTE, name: 'palette' },
  { id: TOOLS.ERASER, name: 'eraser' },
];
