import * as React from 'react';
import styled from 'styled-components';
import { get as _get } from 'lodash';

import { styledTitle } from './NotificationSetup.style';
import { components } from 'ioc/ioc-client';
import { TYPES } from 'src/ioc/TYPES';
import { commonContainer } from 'src/ioc/ioc-common';
import { IAppActionCreators } from 'src/ioc/ioc-interfaces';
import { connect } from 'react-redux';
import webLocalStorage from '../../../services/local-storage';
import { ActionCreator, AnyAction } from 'redux';

const actionCreators = commonContainer.get<IAppActionCreators>(TYPES.ActionCreators);

const Title = styled.div`
  ${styledTitle};
`;

export interface INotificationSetupDispatchProps {
    toggleSoundOnNewMessage: (value: boolean) => void;
    syncChatConfig: ActionCreator<AnyAction>;
}

export interface INotificationSetupStateProps {
    playSoundOnNewMessage: boolean;
    userId: string;
}

export type INotificationSetupProps = INotificationSetupStateProps & INotificationSetupDispatchProps;

/**
 * UI
 */
export class NotificationSetup extends React.Component<INotificationSetupProps> {
    public componentDidMount() {
        webLocalStorage.setStorageKey(this.props.userId);
        this.props.syncChatConfig();
    }

    public render() {
        const { UI } = components;

        return (
            <React.Fragment>
                <Title>Chat message sound</Title>
                <UI.Checkbox
                    value={this.props.playSoundOnNewMessage}
                    onChange={this.props.toggleSoundOnNewMessage}
                    label="Play sound when receiving a new chat message"
                />
            </React.Fragment>
        );
    }
}

export default connect<INotificationSetupStateProps, INotificationSetupDispatchProps>(
    (state: any) => {
        const playSoundOnNewMessage = _get(state, 'room.chat.config.playSoundOnNewMessage');
        const userId = _get(state, 'auth.user.id');

        return {
            playSoundOnNewMessage,
            userId,
        };
    },
    {
        toggleSoundOnNewMessage: actionCreators.configToggleSoundMessage as any,
        syncChatConfig: actionCreators.syncChatConfig as any,
    }
)(NotificationSetup);
