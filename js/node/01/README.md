# Matter streaming

Components from social music streaming platform 
https://app.matter.online/

*Note:* Original file names might be modified.

## Tech stack
- NodeJS v10.15.1
- Koa 2.5.0
- PostgreSQL 9
