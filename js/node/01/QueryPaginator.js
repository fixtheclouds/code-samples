// src/db/QueryPaginator.js
// Custom query paginator to work this knex

const { isNil } = require('lodash')
const knex = require('./connection')

const NotFoundError = require('../errors/NotFoundError')

class QueryPaginator {
  constructor(knexQuery, limit = 10, sort, dir = 'desc') {
    this.limit = limit < 0 ? undefined : limit
    this._sort = sort
    this.dir = dir
    this.originalQuery = knexQuery.clone()
  }

  get sort() {
    if (isNil(this._sort)) return
    const [column, table] = this._sort.split('.').reverse()
    if (table) {
      return this._sort
    }
    return `${this.originalQuery._single.table}.${column}`
  }

  setQuery(query) {
    this.originalQuery = query
  }

  setLimit(limit) {
    this.limit = limit
  }

  getPaginatedQuery(pageNumber) {
    const offset = (pageNumber - 1) * this.limit
    const clonedQuery = this.originalQuery
      .clone()
      .limit(this.limit)
      .offset(offset)
    if (!isNil(this.sort)) {
      clonedQuery.clearOrder().orderBy(this.sort, this.dir)
    }
    return clonedQuery
  }

  async getItemsCount() {
    const [{ count }] = await knex
      .count('* as count')
      .from(this.originalQuery.clone().as('inner'))
    return count
  }

  async getPagesCount(totalCount) {
    return Math.ceil(totalCount / this.limit) || 1
  }

  async getPage(pageNumber = 1) {
    const totalRecords = await this.getItemsCount()
    const pagesCount = await this.getPagesCount(totalRecords)
    if (pageNumber > pagesCount) {
      throw new NotFoundError(`Page with number ${pageNumber} is not found`)
    }
    const pageItems = await this.getPaginatedQuery(pageNumber)
    return new QueryPaginatorPage(pageItems, {
      pageNumber,
      lastPageNumber: pagesCount,
      totalRecords
    })
  }
}

class QueryPaginatorPage {
  constructor(items, paginationData) {
    this.items = items
    const { pageNumber, lastPageNumber, totalRecords } = paginationData
    this.pageNumber = pageNumber
    this.lastPageNumber = lastPageNumber
    this.totalRecords = totalRecords
  }

  get meta() {
    return {
      last_page_number: this.lastPageNumber,
      page_number: this.pageNumber,
      has_next_page: this.hasNextPage,
      has_previous_page: this.hasPreviousPage,
      total_records: this.totalRecords
    }
  }

  get hasNextPage() {
    return this.pageNumber < this.lastPageNumber
  }

  get hasPreviousPage() {
    return this.pageNumber > 1
  }
}

module.exports = { QueryPaginator, QueryPaginatorPage }
