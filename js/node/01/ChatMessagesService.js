// src/services/ChatMessagesService.js
// Send email notifications on unread chats (for background processing)

const { injectable, inject } = require('inversify')
const autobind = require('autobind-decorator').default
const { memoize, groupBy } = require('lodash')
const moment = require('moment-timezone')

const SCHEDULES = ['DAILY', 'WEEKLY']
const SENDING_HOUR = 12
const NOTIFICATION_DELAY = 24 // hours
const REPEAT_TIMEOUT = 72 // hours
const ACTION_TYPE = 'UNREAD_CHATS'

@autobind
@injectable()
class ChatMessagesService {
  @inject('chatsMembersCollection') chatsMembersCollection
  @inject('usersCollection') usersCollection
  @inject('emailNotificationsCollection') emailNotificationsCollection

  @inject('mailerService') mailerService
  @inject('timezoneService') timezoneService

  constructor() {
    this.timestampFor = memoize(this.timestampFor)
  }

  async notifyOnUnread() {
    const currentTime = moment().startOf('hour')

    const unreadChats = await this.chatsMembersCollection
      .get()
      .where('unread_count', '>', 0)

    const groupedChats = groupBy(unreadChats, 'sender_id')

    const recipients = await this.getRecipients(Object.keys(groupedChats))

    const recipientData = []
    recipients.forEach(recipient => {
      const { id, country, city, email, display_name: displayName } = recipient
      const schedule = recipient.email_on_chat_messages
      const timeZone = this.timezoneService.decodeByLocation(city, country)
      const count = groupedChats[id].length

      if (this.timestampFor(timeZone)[schedule].isSame(currentTime)) {
        recipientData.push({ id, count, email, displayName })
      }
    })

    await this.sendMail(recipientData)
    for (const item of recipientData) {
      await this.createEmailNotification(item)
    }
  }

  async sendMail(recipientData) {
    try {
      await this.mailerService.notifyOnUnreadChats(recipientData)
    } catch (error) {
      console.error('Error on email sending', error)
    }
  }

  async getRecipients(userIds) {
    const delayTime = moment()
      .subtract(NOTIFICATION_DELAY, 'hours')
      .startOf('hour')
    const repeatTime = moment()
      .subtract(REPEAT_TIMEOUT, 'hours')
      .startOf('hour')

    return this.usersCollection
      .get(userIds)
      .whereIn('email_on_chat_messages', SCHEDULES)
      .whereRaw(
        `users.id IN (
          SELECT sender_id 
          FROM chats_members 
          INNER JOIN chats_messages ON chats_messages.chat_id = chats_members.chat_id 
          GROUP BY chats_members.sender_id 
          HAVING MAX(chats_messages.created_at) < ?
        )`,
        [delayTime]
      )
      .whereRaw(
        `(email_on_chat_messages <> 'DAILY'
          OR NOT EXISTS (
            SELECT * 
            FROM email_notifications
            WHERE user_id = users.id
            AND email_notifications.created_at > ?
          )
        )`,
        [repeatTime]
      )
  }

  createEmailNotification({ id }) {
    return this.emailNotificationsCollection.insert({
      action_type: ACTION_TYPE,
      user_id: id
    })
  }

  timestampFor(timeZone) {
    return {
      WEEKLY: moment
        .tz(timeZone)
        .startOf('week')
        .set('hour', SENDING_HOUR)
        .startOf('hour'),
      DAILY: moment.tz(SENDING_HOUR, 'HH', timeZone).startOf('hour')
    }
  }
}

module.exports = ChatMessagesService
