// src/controllers/admin/GenresController.js
// Controls the logic of Genre management pages for admin app

const assert = require('assert')
const autobind = require('autobind-decorator').default
const { injectable, inject } = require('inversify')
const { isNil } = require('lodash')

const { QueryPaginator } = require('../../db/Paginator')
const { JSONAPISerializer } = require('../../decorators/JSONAPISerializer')
const { Paginated } = require('../../decorators/Paginated')
const { ValidatePayload } = require('../../decorators/ValidatePayload')
const { GENRE_SCHEMA } = require('../../ResponseSchemas')
const NotFoundError = require('../../errors/NotFoundError')
const ValidationError = require('../../errors/ValidationError')

@injectable()
@autobind
class GenresController {
  @inject('genresCollection') genresCollection
  @inject('filesCollection') filesCollection

  @inject('genresService') genresService

  @Paginated()
  @JSONAPISerializer(GENRE_SCHEMA)
  async index(ctx) {
    const { isPaginated, limit, page, sort, dir } = ctx.state.pagination
    const query = this.genresCollection.get()
    return isPaginated ? new QueryPaginator(query, limit, sort, dir).getPage(page) : query
  }

  @ValidatePayload({
    fields: {
      name: { type: 'string', required: true },
      background_image_id: { type: 'relationship', relationshipType: 'has_one' },
      icon_id: { type: 'relationship', relationshipType: 'has_one' }
    },
    permit: ['name', 'background_image_id', 'icon_id']
  })
  @JSONAPISerializer(GENRE_SCHEMA)
  async create(ctx) {
    const { payload } = ctx.state
    payload.name = payload.name.toLowerCase()

    assert(
      isNil(payload.background_image_id) ||
        (await this.filesCollection.exists(payload.background_image_id)),
      new NotFoundError('File', payload.background_image_id)
    )

    assert(
      isNil(payload.icon_id) || (await this.filesCollection.exists(payload.icon_id)),
      new NotFoundError('File', payload.icon_id)
    )

    const existingGenre = await this.genresCollection
      .get()
      .whereRaw('LOWER(name) = ?', payload.name)
      .first()

    assert(
      isNil(existingGenre),
      new ValidationError(`A Genre with name "${payload.name}" already exists`)
    )

    return this.genresCollection.create(payload)
  }

  @ValidatePayload({
    fields: {
      background_image_id: { type: 'relationship', relationshipType: 'has_one' },
      icon_id: { type: 'relationship', relationshipType: 'has_one' }
    },
    permit: ['background_image_id', 'icon_id']
  })
  @JSONAPISerializer(GENRE_SCHEMA)
  async update(ctx) {
    const { payload } = ctx.state
    const { id } = ctx.params

    assert(await this.genresCollection.exists(id), new NotFoundError('Genre', id))

    assert(
      isNil(payload.background_image_id) ||
        (await this.filesCollection.exists(payload.background_image_id)),
      new NotFoundError('File', payload.background_image_id)
    )

    assert(
      isNil(payload.icon_id) || (await this.filesCollection.exists(payload.icon_id)),
      new NotFoundError('File', payload.icon_id)
    )

    return this.genresCollection.update(id, payload)
  }

  async delete(ctx) {
    const { id: genreId } = ctx.params

    assert(
      await this.genresCollection.exists(genreId),
      new NotFoundError('Genre', genreId)
    )

    await this.genresCollection.delete(genreId)
    ctx.status = 200
  }

  @ValidatePayload({
    fields: {
      ids: { type: 'array', min: 1 }
    },
    permit: ['ids'],
    require: ['ids']
  })
  async updateOrder(ctx) {
    const { ids } = ctx.state.payload

    const missingIds = await this.genresCollection.missing(ids)
    assert(
      missingIds.length === 0,
      new NotFoundError(`Genres with ids ${missingIds.join(', ')} are not found`)
    )

    await this.genresService.updateOrder(ids)
    ctx.status = 200
  }
}

module.exports = GenresController
