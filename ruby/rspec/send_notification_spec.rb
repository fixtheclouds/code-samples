# frozen_string_literal: true

# spec/services/telegram/send_notification_spec.rb
describe Telegram::SendNotification do
  describe '#call' do
    subject { described_class.new.call(message) }

    let(:message) { Faker::Lorem.word }
    let(:stub_response) { File.read('spec/fixtures/telegram/message_sent.json') }

    before do
      allow(Rails).to receive(:env).and_return(ActiveSupport::StringInquirer.new('production'))
      stub_request(:post, %r{api.telegram.org/bot}).to_return(status: 200, body: stub_response)
    end

    it 'delegates message to TG client' do
      expect_any_instance_of(Telegram::Client).to receive(:send_message).with(
        Settings.telegram.chats.notifications,
        message
      ).once

      subject
    end
  end
end
