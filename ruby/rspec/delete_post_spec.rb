# frozen_string_literal: true

# spec/graphql/mutations/delete_post_spec.rb
# Test graphQL mutation for post deletion
describe Mutations::DeletePost, type: :request do
  include Rack::Test::Methods
  include RequestSpecHelper # Adds #sign_in method

  describe '#resolve' do
    subject { post '/graphql', { query: query, variables: variables } }

    before { sign_in(user) }

    let(:user) { create(:user) }
    let(:group) { create(:group) }
    let(:comment) { create(:post, owner: group) }
    let(:variables) { { id: comment.id } }
    let(:query) do
      <<-GQL
        mutation DeletePostMutation($id: ID!) {
          deletePost(input: { postId: $id }) {
            ok
            errors {
              messages
            }
          }
        }
      GQL
    end

    it 'returns correct data' do
      subject

      aggregate_failures 'expected results' do
        data = JSON.parse(last_response.body)
        expect(data.dig('data', 'deletePost', 'ok')).to eq true
        expect(data['errors']).to be_nil
      end
    end

    it 'deletes a post' do
      expect { subject }.to change(comment.reload, :state).to Post::DELETED
    end

    context 'when deleted' do
      let(:comment) { create(:post, state: Post::DELETED, owner: group) }

      it 'returns correct data' do
        subject

        aggregate_failures 'expected results' do
          data = JSON.parse(last_response.body)
          expect(data.dig('data', 'deletePost', 'ok')).to be_nil
          expect(data.dig('data', 'deletePost', 'errors', 'messages')).to eq 'Not found'
        end
      end

      it 'does not change a post' do
        expect { subject }.not_to change(comment.reload, :state)
      end
    end

    context 'when no access' do
      let(:user) { create(:user) }

      it 'returns correct data' do
        subject

        aggregate_failures 'expected results' do
          data = JSON.parse(last_response.body)
          expect(data.dig('data', 'deletePost', 'ok')).to be_nil
          expect(data['errors']).to include hash_including(
            'message' => 'You are not authorized to perform this action'
          )
        end
      end

      it 'does not change a post' do
        expect { subject }.not_to change(comment.reload, :state)
      end
    end
  end
end
