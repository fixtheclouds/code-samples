# frozen_string_literal: true

# spec/services/groups/calculate_investment_amount_spec.rb
describe Groups::CalculateInvestmentAmount do
  describe '#call' do
    subject { described_class.new.call(group) }

    let(:group) { create(:group) }
    let(:deal) { create(:deal, :live) }
    let(:members) { create_list(:groups_membership, 3, group: group) }
    let!(:investments) do
      members.map.with_index do |member, index|
        create(:investment, user: member.user, amount_cents: 100_000 * (index + 1), deal: deal)
      end
    end

    before { group.deals << deal unless self.class.metadata[:skip_before] }

    context 'when investments belong to portfolio' do
      it { is_expected.to eq 600_000 } # 100,000 + 200,000 + 300,000 cents
    end

    context 'when one investment does not belong to portfolio' do
      let(:deal2) { create(:deal, :published) }
      let!(:investments) do
        members.map.with_index do |member, index|
          create(
            :investment,
            user: member.user,
            amount_cents: 100_000 * (index + 1),
            deal: index > 1 ? deal2 : deal
          )
        end
      end

      it { is_expected.to eq 300_000 }
    end

    context 'when one investor is not a member' do
      it do
        group.memberships.first.destroy
        is_expected.to eq 500_000
      end
    end

    context 'when first investment is cancelled' do
      it do
        investments.first.update(state: Investment::CANCELLED)
        is_expected.to eq 500_000
      end
    end

    context 'when no deals in portfolio', skip_before: true do
      it { is_expected.to eq 0 }
    end
  end
end
