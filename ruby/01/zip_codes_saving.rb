# lib/services/parsing_jobs/zip_codes/saving.rb
# Used to parse zip codes from Excel and optionally blacklist customers by zip
module Services
  module ParsingJobs
    module ZipCodes
      class Saving < ::Services::ParsingJobs::Saving
        def initialize(job)
          super
          @percent = @job.percent
          @codes = Set.new
        end

        def headers
          {
            zip: 'Zip'
          }
        end

        private

        def processing_rows
          each_row { |row| @codes << row[headers[:zip]].to_s }
          ZipCode.transaction do
            destroy_zip_codes
            create_zip_codes
            blacklist_customers
          end
        end

        def create_zip_codes
          @item = ZipCode.new(codes: @codes.to_a, percent: @percent, user: @job.user, parsing_job: @job)
          @item.save!
        rescue
          process_errors(@item)
          raise ActiveRecord::Rollback
        end

        def destroy_zip_codes
          Services::ZipCodes::Destroy.new.call
        end

        def blacklist_params
          {
            status: 'Blacklisted zip code',
            description: 'Blacklisted zip code'
          }
        end

        def blacklist_customers
          return unless Settings::Base.get('Blacklist by zip')
          limit = (customers_query.count / 100.0 * @percent.to_i).floor
          customer_ids = customers_query.order('RANDOM()').limit(limit).pluck(:id)

          Services::Blacklists::MassCreate.new(customer_ids, blacklist_params, @job.user).call
        end

        def customers_query
          Customer
            .default_class
            .clients
            .left_joins(:orders)
            .where(orders: { id: nil })
            .where("details->'zip' IN (?)", @codes)
        end
      end
    end
  end
end
