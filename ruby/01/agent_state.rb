# app/models/agent_state.rb
# Represents customer agent state for certain period of time
class AgentState < ApplicationRecord

  belongs_to :campaign, class_name: 'Campaigns::Agent'
  belongs_to :customer

  has_one :business_unit, through: :campaign

  has_many :campaign_types, through: :customer, source: :agent_campaign_types
  has_many :feedbacks, class_name: 'Feedbacks::Base', dependent: :restrict_with_error

  validates :campaign, :customer, :period, presence: true
  validates :campaign_id, uniqueness: { scope: [:customer_id, :period] }

  delegate :staff?, to: :customer, prefix: true

  state_machine initial: :candidate do
    state :candidate do
      validate :check_not_staff
    end
    state :renewal, :active, :refused
  end

  scope :current,            -> { where(period: current_period) }
  scope :not_blacklisted,    -> { includes(customer: :blacklists).where(blacklists: { id: nil }) }
  scope :by_business_unit,   -> (bu) { joins(:campaign).where(campaigns: { business_unit_id: bu.id }) }
  scope :with_person_states, -> (*state) { joins(:customer).where(customers: { person_state: state }) }
  scope :without_critical_complaints, -> do
    where("NOT EXISTS (SELECT 1 FROM contacts WHERE customer_id = agent_states.customer_id AND request_type = 'Complaint' AND critical)")
  end
  scope :by_campaign_type, -> (company_id, type, tr_group) do
    condition = <<~SQL
      agent_campaign_types.id IS NULL
      OR (
        1 = 1
        AND agent_campaign_types.company_id = :company_id
        AND agent_campaign_types.campaign_types @> ARRAY[:type]::varchar[]
        #{'AND agent_campaign_types.campaign_tr_groups @> ARRAY[:tr_group]::integer[]' if tr_group}
      )
    SQL

    joins('LEFT JOIN agent_campaign_types ON agent_campaign_types.customer_id = agent_states.customer_id')
      .where(condition, company_id: company_id, type: type, tr_group: tr_group)
  end

  class << self
    def current_period
      Settings::Base.get('Current agent period') || 0
    end
  end

  def current?
    period == self.class.current_period
  end

  def has_activity?
    actual_feedback = feedbacks.without_state(:no_information).exists?
    (current? && (active? || (refused? && actual_feedback))) ||
      ((active? || refused?) && actual_feedback)
  end

  private

  def check_not_staff
    errors.add(:state, :not_for_staff, state: 'Candidate') if customer_staff?
  end
end
