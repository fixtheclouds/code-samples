# lib/services/parsing_jobs/saving.rb
# Custom Excel parser used in background processing (Base class)
module Services
  module ParsingJobs
    class Saving
      include Services::Mixins::ParsingJobs::ErrorsWithLineNumbers

      attr_reader :book
      attr_writer :rows
      attr_accessor :errors

      def initialize(job)
        @job = job
        @errors = []
      end

      def call
        parse_excel if @rows.blank?
        processing_rows
        save_errors
      end

      def parse_excel
        uploader = ExcelUploader.new
        uploader.download! @job.source_file_url
        raise ArgumentError, I18n.t('lib.parser.empty_file') unless uploader.file
        @book = Mercury::BatchFactory.build(uploader.file.path, excel_options)
      end

      def rows
        @rows ||= book.rows
      end

      private

      def excel_options
        { headers: headers.values.presence }
      end

      def process_errors(object, index = nil)
        index ||= @index
        errors = object.errors.keys.map do |key|
          object.errors.full_messages_for(key).map do |message|
            suppress(NoMethodError) do
              message = "#{message}: #{object.instance_eval(key.to_s)}" unless object.instance_eval(key.to_s).nil?
            end
            { message: message, index: index }.compact
          end
        end
        @errors += errors.flatten
      end

      def headers
        {}
      end

      def processing_rows
        raise NotImplementedError
      end

      def save_errors
        @job.update_column(:messages, @errors)
      end

      def prepare_attributes(row)
        row.transform_keys(&headers.invert).reject { |_, v| v.blank? }
      end
    end
  end
end
