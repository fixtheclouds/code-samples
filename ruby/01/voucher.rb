# app/models/voucher.rb
class Voucher < ApplicationRecord
  include Shared::AuditTransitions

  belongs_to :customer
  belongs_to :creator, class_name: 'User'
  belongs_to :linked_by, class_name: 'User'
  belongs_to :order
  belongs_to :origin_order, class_name: 'Order'
  belongs_to :linked_order, class_name: 'Order'
  belongs_to :action, class_name: 'Business::Action'

  has_many :parsing_job_entities, as: :entity, dependent: :delete_all
  has_many :parsing_jobs, through: :parsing_job_entities, as: :entity, class_name: 'ParsingJobs::Base'

  audited associated_with: :customer

  validates :code, :expiry_date, presence: true
  validates :code, uniqueness: { case_sensitive: false }
  validates :amount_cents, numericality: { greater_than: 0 }
  validates :origin_order_id, uniqueness: true, allow_blank: true

  monetize :amount_cents

  state_machine initial: :pending, audit: true do
    after_transition on: :link, do: :touch_linked_at
    after_transition on: :unlink, do: :undo_link
    after_transition on: :disuse, do: :nullify_order

    state :pending do
      transition to: :linked, on: :link
    end

    state :linked do
      validates :customer, presence: true

      transition to: :used, on: :use
    end

    state :used do
      validates :order, presence: true
    end

    event :disuse do
      transition :used => :linked
    end

    event :unlink do
      transition :linked => :pending
    end
  end

  define_date_scopes :expiry_date

  private

  def undo_link
    update_columns(customer_id: nil, linked_at: nil, linked_by_id: nil)
  end

  def nullify_order
    update_column :order_id, nil
  end

  def touch_linked_at
    touch :linked_at
  end
end
